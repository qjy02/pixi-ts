import { MapCourse } from '@/model/stage';

const p2Course: MapCourse[] = [
  {
    courseName: 'Java 语言入门',
    id: 'pro_j1',
    isLocked: true,
    isPreSale: false,
    courseId: 'j1',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'Java 面向对象',
    id: 'pro_j2',
    isLocked: true,
    isPreSale: false,
    courseId: 'j2',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'Java 网络编程',
    id: 'pro_j3',
    isLocked: true,
    isPreSale: false,
    courseId: 'j3',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: '实战音乐评论分析',
    id: 'pro_j4',
    isLocked: true,
    isPreSale: false,
    courseId: 'j4',
    // courseChapters: [],
    // currentChapter: 0,
  },
];

const p3Course: MapCourse[] = [
  {
    courseName: 'Web 前端基础',
    id: 'pro_f1',
    isLocked: true,
    isPreSale: false,
    courseId: 'f1',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'Web 前端进阶',
    id: 'pro_f2',
    isLocked: true,
    isPreSale: false,
    courseId: 'f2',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'Web 前端项目实战',
    id: 'pro_f3',
    isLocked: true,
    isPreSale: false,
    courseId: 'f3',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'JavaScript/TypeScript',
    id: 'pro_f4',
    isLocked: true,
    isPreSale: false,
    courseId: 'f4',
    // courseChapters: [],
    // currentChapter: 0,
  },
  {
    courseName: 'JavaScript 项目实战',
    id: 'pro_f10',
    isLocked: true,
    isPreSale: false,
    courseId: 'f10',
    // courseChapters: [],
    // currentChapter: 0,
  },
];

export { p2Course, p3Course };
