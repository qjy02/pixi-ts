import { stageList, imgPre } from './config';
import * as PIXI from 'pixi.js';

export default function textContainer(studyPoint: any) {
  const textContainer = new PIXI.Container();
  const box = PIXI.Sprite.from(imgPre + '/common/button.png');
  box.x = 47;
  box.y = 76;
  const circle = new PIXI.Graphics();
  circle.lineStyle(2, 0xffffff, 1);
  circle.beginFill(0x619295, 1);
  circle.drawCircle(0, 0, 25);
  circle.endFill();
  const basicText = new PIXI.Text(`${studyPoint.courseName}`, {
    fontSize: 34,
    fontWeight: 'bold',
    fill: ['#604f17'],
  });
  basicText.x = 42;
  basicText.y = -18;
  let progressNumber =
    studyPoint.courseProgress === 0 ? '0' : `${studyPoint.courseProgress}%`;
  const progress = new PIXI.Text(`课程进度: ${progressNumber}`, {
    fontSize: 20,
    fontWeight: 'bold',
    fill: ['#604f17'],
  });
  progress.x = 47;
  progress.y = 36;
  const number = new PIXI.Text('1', {
    fontSize: 30,
    fontWeight: 'bold',
    fill: ['#ffffff'],
  });
  number.x = -8;
  number.y = -16;

  const noticeText = new PIXI.Text('温故知新', {
    fontSize: 18,
    fill: ['#ffffff'],
  });
  noticeText.x = 60;
  noticeText.y = 82;

  textContainer.addChild(box, circle, basicText, progress, number, noticeText);
  return textContainer;
}
// import initGUI, { guiText } from '../scripts/datgui';
// const rem = 0.26;
// const userLists = [
//     {
//         index:'1',
//         className:"Java语言入门",
//         classfinish:"课程进度: 100%",
//         finished:true,
//         positionX:100,
//         positionY:100
//     },
//     {
//         index:'2',
//         className:"Java面向对象",
//         classfinish:"课程进度：100%",
//         finished:true,
//         positionX:200,
//         positionY:200
//     },
//     {
//         index:'3',
//         className:"Java网络编程",
//         classfinish:"课程进度：100%",
//         finished:true,
//         positionX:300,
//         positionY:300
//     },{
//         index:'4',
//         className:"实战音乐评论",
//         classfinish:"课程进度：18%",
//         finished:false,
//         positionX:400,
//         positionY:400
//     }

// ];
// export default function text(app:any){

//     const styleNum = new PIXI.TextStyle({
//         fontSize: 30*rem,
//         fontWeight: 'bold',
//         fill:['#ffffff']
//     });

//     const style2 = new PIXI.TextStyle({
//         fontSize:18*rem,
//         fill:['#ffffff'],
//     });

//     for(let userList of userLists){
//         const container = new PIXI.Container();
//         // this.dragSprite(container);
//         if(userList.finished===false){
//             const style = new PIXI.TextStyle({
//                 fontSize: 43*rem,
//                 fontWeight: 'bold',
//                 fill: ['#4b6ecb'], // gradient·
//             });
//             const style1 = new PIXI.TextStyle({
//                 fontSize:20*rem,
//                 fontWeight:'bold',
//                 fill:['#4b6ecb'],
//             });

//             const basicText = new PIXI.Text(userList.className,style);
//             const basicText1 = new PIXI.Text(userList.classfinish,style1);
//             const number = new PIXI.Text(userList.index,styleNum);
//             const basicText2 = new PIXI.Text('开始学习',style2);
//             const circle = new PIXI.Graphics();
//             const box = PIXI.Sprite.from(imgPre+'/common/button.png');
//             box.scale.set(rem);

//             circle.lineStyle(4*rem,0xffffff,1);
//             circle.beginFill(0x4861dc,1);
//             circle.drawCircle(0,0,30*rem);
//             // circle.anchor.set(0);
//             circle.endFill();

//             box.x= 47*rem;
//             box.y=76*rem;
//             basicText.x = 42*rem;
//             basicText.y = -25*rem;
//             basicText1.x = 47*rem;
//             basicText1.y = 36*rem;
//             basicText2.x = (box.x+24*rem);
//             basicText2.y = (box.y+8*rem);

//             number.x = -8*rem;
//             number.y = -16*rem;

//             container.x = userList.positionX;
//             container.y = userList.positionY;

//             container.addChild(basicText);
//             container.addChild(basicText1);
//             container.addChild(circle);
//             container.addChild(number);
//             container.addChild(box);
//             container.addChild(basicText2);
//             // container.scale.set(0.26);
//             app.stage.addChild(container);
//             app.addChild(container)
//         }else{
//                 const style = new PIXI.TextStyle({
//                 fontSize: 43*rem,
//                 fontWeight: 'bold',
//                 fill: ['#604f17'], // gradient·

//             });
//             const style1 = new PIXI.TextStyle({
//                 fontSize:20*rem,
//                 fontWeight:'bold',
//                 fill:['#604f17'],
//             });
//             const basicText = new PIXI.Text(userList.className,style);
//             const basicText1 = new PIXI.Text(userList.classfinish,style1);
//             const number = new PIXI.Text(userList.index,styleNum);
//             const basicText2 = new PIXI.Text('温故知新',style2);
//             const circle = new PIXI.Graphics();
//             const box = PIXI.Sprite.from(imgPre+'/common/button.png');
//             box.scale.set(rem);

//             circle.beginFill(0x3d5c61,1);
//             circle.drawCircle(0,0,30*rem);
//             // circle.anchor.set(0);
//             circle.endFill();

//             box.x= 47*rem;
//             box.y=76*rem;
//             basicText.x = 42*rem;
//             basicText.y = -25*rem;
//             basicText1.x = 47*rem;
//             basicText1.y = 36*rem;
//             basicText2.x = (box.x+24*rem);
//             basicText2.y = (box.y+8*rem);

//             number.x = -8*rem;
//             number.y = -16*rem;

//             container.x = userList.positionX;
//             container.y = userList.positionY;
//             container.addChild(basicText);
//             container.addChild(basicText1);
//             container.addChild(circle);
//             container.addChild(number);
//             container.addChild(box);
//             container.addChild(basicText2);
//             // container.scale.set(0.26);
//             app.stage.addChild(container);
//             // app.addChild(container);
//             }
//         }
//     }
