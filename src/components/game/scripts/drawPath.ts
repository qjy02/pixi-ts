import * as PIXI from 'pixi.js';

export default function drawPath(
  pathStr: string,
  fill = 0xff3300,
  lineStyle: { width: number; color: number } = { width: 2, color: 0x000000 }
) {
  const shape = new PIXI.Graphics();
  shape.beginFill(fill);
  shape.lineStyle(lineStyle.width, lineStyle.color);
  const pathArr = getPathPoints(pathStr);

  pathArr.forEach((path: any) => {
    // 画图
    if (path.type.toLowerCase() === 'm') {
      shape.moveTo(path.points[0][0], path.points[0][1]);
    } else if (
      path.type.toLowerCase() === 'l' ||
      path.type.toLowerCase() === 'h'
    ) {
      shape.lineTo(path.points[0][0], path.points[0][1]);
    } else if (path.type.toLowerCase() === 's') {
      shape.quadraticCurveTo(
        path.points[0][0],
        path.points[0][1],
        path.points[1][0],
        path.points[1][1]
      );
    } else if (path.type.toLowerCase() === 'c') {
      shape.bezierCurveTo(
        path.points[0][0],
        path.points[0][1],
        path.points[1][0],
        path.points[1][1],
        path.points[2][0],
        path.points[2][1]
      );
    } else if (path.type === 'z') {
      shape.lineTo(pathArr[0].points[0][0], pathArr[0].points[0][1]);
    }
  });
  shape.closePath();
  shape.endFill();
  return shape;
}

// todo 加参数 pathStr
function getPathPoints(pathStr: string) {
  const reg = new RegExp(/(m|c|s|l|z)([^mcslz]*)/gi);
  // replace 一遍有遗漏，重复一遍
  pathStr = pathStr
    .replace(/(\d)(-\d)/g, '$1,$2')
    .replace(/(\d)(-\d)/g, '$1,$2')
    // .replace(/(\d)\s*(-?\d)/g, '$1,$2')
    .replace(/\s|\n|\r/g, '');
  const matchArr = Array.from(pathStr.matchAll(reg));
  let pathArr: any[] = [];
  // matchArr = [{type: 'M', points:[0,0]},...matchArr.slice(28)]
  matchArr.forEach((item: RegExpMatchArray, idx: number) => {
    if (item.length > 0) {
      // pathMap[`${item[1]}-${idx}`] = item[2].split(',')
      const temp = item[2].split(/,/g);
      const res: any[] = [];

      if (item[1] === 'h') {
        res.push([+item[2], 0]);
      } else if (item[1] === 'H') {
        let y: number = 0;
        if (idx > 0) {
          const lastP = matchArr[idx - 1];
          const lastPTemp = lastP[2].split(/,/g);
          if (lastPTemp.length > 0) {
            const y = lastPTemp.pop() !== undefined ? +lastPTemp.pop()! : 0;
          }
        }
        res.push([+item[2], y]);
      } else if (item[1] !== 'z') {
        temp.forEach((p, i) => {
          if (i % 2 === 0) {
            res.push([+p, +temp[i + 1]]);
          }
        });
      }

      pathArr.push({
        type: item[1],
        // points: item[2].split(' ').map((e) => e.split(',').map((e) => +e)),
        points: res,
      });
    }
  });

  pathArr = absPath(pathArr);
  return pathArr;
}

function absPath(pathArr: any[]) {
  pathArr.forEach((path: any, idx: number, arr: any[]) => {
    if (
      path.type === 'C' ||
      path.type === 'S' ||
      path.type === 'L' ||
      path.type === 'H'
    ) {
      return path;
    }
    // 上一次路径的终点
    let lastP: any;
    if (idx) {
      const tempP = [...arr[idx - 1].points];
      lastP = tempP.splice(tempP.length - 1);
    } else {
      lastP = [[0, 0]];
    }

    // 相对位置转为绝对位置
    path.points = path.points.map((o: any) => {
      for (let index = 0; index < o.length; index++) {
        if (index % 2) {
          o[index] += lastP[0][1];
        } else {
          o[index] += lastP[0][0];
        }
      }
      return o;
    });
  });

  return pathArr;
}
