import { StageList } from '@/model/stage';
import { p2Course, p3Course } from './courseConfig';
import * as svgPath from './svgPath';

// StageCourse
const imgPre = 'https://style.youkeda.com/img/ice/user-map';

const stageList: StageList = {
  stages: [
    {
      id: 'pro_p2',
      stageType: 'P2',
      stageName: '爬虫工程师',
      stageCourses: p2Course,
      isdeveloping: false,
      bought: true,
      source: {
        position: [104, 300],
        buyCoursePosition: [294, 458],
        background: 'stage-p2.png',
        lockStagePosition: [36, -1],
        lockStage: 'stage-p2-lock.png',
        waves: {
          wave1: {
            position: [18, 502],
            pic: 'p2_1.png',
          },
          wave2: {
            position: [35, 672],
            pic: 'p2_2.png',
          },
          wave3: {
            position: [-11, 802],
            pic: 'p2_3.png',
          },
          wave4: {
            position: [-11, 862],
            pic: 'p2_4.png',
          },
          wave5: {
            position: [99, 892],
            pic: 'p2_5.png',
          },
          wave6: {
            position: [329, 1042],
            pic: 'p2_6.png',
          },
          wave7: {
            position: [356, 1112],
            pic: 'p2_7.png',
          },
          wave8: {
            position: [598, 1152],
            pic: 'p2_8.png',
          },
          wave9: {
            position: [640, 1142],
            pic: 'p2_9.png',
          },
          wave10: {
            position: [756, 1122],
            pic: 'p2_10.png',
          },
          wave11: {
            position: [949, 482],
            pic: 'p2_11.png',
          },
          wave12: {
            position: [942, 422],
            pic: 'p2_12.png',
          },
          wave13: {
            position: [982, 352],
            pic: 'p2_13.png',
          },
          wave14: {
            position: [1041, 202],
            pic: 'p2_14.png',
          },
          wave15: {
            position: [979, 182],
            pic: 'p2_15.png',
          },
        },
        studyPoint1: {
          courseName: 'Java语言入门',
          courseProgress: 0,
          pointPosition: [326, 222],
        },
        studyPoint2: {
          courseName: 'Java面向对象',
          courseProgress: 0,
          pointPosition: [184, 700],
        },
        studyPoint3: {
          courseName: 'Java网络编程',
          courseProgress: 0,
          pointPosition: [476, 902],
        },
        studyPoint4: {
          courseName: '实战音乐评论',
          courseProgress: 0,
          pointPosition: [501, 442],
        },
        part1: {
          lockMask: 'part1.png',
          lockPosition: [164, -3],
          path: svgPath.p2path1,
          hoverMask: 'p2-part1-hover.png',
          hoverPosition: [185, 30],
          maskPosition: [184, 30],
        },
        part2: {
          lockMask: 'part2.png',
          hoverMask: 'p2-part2-hover.png',
          path: svgPath.p2path2,
          hoverPosition: [105, 453],
          maskPosition: [113, 456],
          lockPosition: [34, 415],
        },
        part3: {
          lockMask: 'part3.png',
          hoverMask: 'p2-part3-hover.png',
          path: svgPath.p2path3,
          hoverPosition: [386, 651],
          maskPosition: [240, 119],
          lockPosition: [343, 650],
        },
        part4: {
          lockMask: 'part4.png',
          hoverMask: 'p2-part4-hover.png',
          path: svgPath.p2path4,
          hoverPosition: [341, 350],
          maskPosition: [241, 118],
          lockPosition: [342, 345],
        },
      },
    },
    {
      id: 'pro_p3',
      stageType: 'P3',
      stageName: '爬虫工程师',
      stageCourses: p3Course,
      isdeveloping: false,
      bought: true,
      source: {
        position: [1065, 174],
        buyCoursePosition: [494, 258],
        background: 'stage-p3.png',
        lockStagePosition: [2, 3],
        lockStage: 'stage-p3-lock.png',
        waves: {
          wave1: {
            position: [1176, 772],
            pic: 'p3_2.png',
          },
          wave2: {
            position: [1365, 652],
            pic: 'p3_3.png',
          },
          wave3: {
            position: [1500, 422],
            pic: 'p3_4.png',
          },
        },
        studyPoint1: {
          courseName: 'Web前端基础',
          courseProgress: 0,
          pointPosition: [254, 522],
        },
        studyPoint2: {
          courseName: 'Web前端进阶',
          courseProgress: 0,
          pointPosition: [353, 182],
        },
        studyPoint3: {
          courseName: 'JavaScript/TypeScript',
          courseProgress: 0,
          pointPosition: [918, 230],
        },
        studyPoint4: {
          courseName: 'Web前端项目实战',
          courseProgress: 0,
          pointPosition: [696, 552],
        },
        studyPoint5: {
          courseName: 'JavaScript实战',
          courseProgress: 0,
          pointPosition: [1093, 590],
        },
        part1: {
          path: svgPath.p3path1,
          lockMask: 'part1.png',
          hoverMask: 'p3-part1-hover.png',
          maskPosition: [0, 277],
          hoverPosition: [3, 282],
          lockPosition: [3, 282],
        },
        part2: {
          lockMask: 'part2.png',
          hoverMask: 'p3-part2-hover.png',
          path: svgPath.p3path2,
          maskPosition: [-1285, -875],
          hoverPosition: [54, 2],
          lockPosition: [54, 2],
        },
        part3: {
          lockMask: 'part3.png',
          hoverMask: 'p3-part3-hover.png',
          path: svgPath.p3path3,
          maskPosition: [701, 68],
          hoverPosition: [710, 73],
          lockPosition: [710, 73],
        },
        part4: {
          lockMask: 'part4.png',
          hoverMask: 'p3-part4-hover.png',
          path: svgPath.p3path4,
          maskPosition: [359, 300],
          hoverPosition: [361, 297],
          lockPosition: [361, 297],
        },
        part5: {
          lockMask: 'part5.png',
          hoverMask: 'p3-part5-hover.png',
          path: svgPath.p3path5,
          maskPosition: [982, 376],
          hoverPosition: [995, 383],
          lockPosition: [995, 383],
        },
      },
    },
    {
      id: 'pro_p4',
      stageType: 'P4',
      stageName: '全栈工程师',
      stageCourses: [],
      isdeveloping: true,
      bought: false,
      source: {
        position: [1168, 860],
        background: 'stage-p4.png',
        waves: {
          wave1: {
            position: [240, 132],
            pic: 'p4_1.png',
          },
          wave2: {
            position: [108, 252],
            pic: 'p4_2.png',
          },
          wave3: {
            position: [62, 302],
            pic: 'p4_3.png',
          },
          wave4: {
            position: [0, 552],
            pic: 'p4_4.png',
          },
          wave5: {
            position: [153, 712],
            pic: 'p4_4_5.png',
          },
          wave6: {
            position: [816, 1052],
            pic: 'p4_5.png',
          },
          wave7: {
            position: [1174, 992],
            pic: 'p4_6.png',
          },
          wave8: {
            position: [1238, 882],
            pic: 'p4_7.png',
          },
          wave9: {
            position: [1311, 112],
            pic: 'p4_8.png',
          },
        },
      },
    },
    {
      id: 'pro_p5',
      stageType: 'P5',
      stageName: '大数据工程师',
      stageCourses: [],
      isdeveloping: true,
      bought: false,
      source: {
        position: [110, 1276],
        background: 'stage-p5.png',
        waves: {
          wave1: {
            position: [34, 22],
            pic: 'p5_1.png',
          },
          wave2: {
            position: [-12, 442],
            pic: 'p5_2.png',
          },
          wave3: {
            position: [3, 662],
            pic: 'p5_3.png',
          },
          wave4: {
            position: [2464, 682],
            pic: 'p5_4.png',
          },
        },
      },
    },
  ],
};

const imgSources: any = {
  common: [
    'https://qgt-style.oss-cn-hangzhou.aliyuncs.com/img/ice/user-map/background-blue.jpg',
    'https://qgt-style.oss-cn-hangzhou.aliyuncs.com/img/ice/user-map/total.jpg',
    `${imgPre}/common/overlay.png`,
    `${imgPre}/common/replacement.png`,
  ],
  p2: [
    'p2_1.png',
    'p2_2.png',
    'p2_3.png',
    'p2_4.png',
    'p2_5.png',
    'p2_6.png',
    'p2_7.png',
    'p2_8.png',
    'p2_9.png',
    'p2_10.png',
    'p2_11.png',
    'p2_12.png',
    'p2_13.png',
    'p2_14.png',
    'p2_15.png',
    'stage-p2.png',
    'stage-p2-lock.png',
    'part1.png',
    'part2.png',
    'part3.png',
    'part4.png',
    'p2-part1-hover.png',
    'p2-part2-hover.png',
    'p2-part3-hover.png',
    'p2-part4-hover.png',
  ],
  p3: [
    'stage-p3.png',
    'p3_1.png',
    'p3_2.png',
    'p3_3.png',
    'p3_4.png',
    'stage-p3-lock.png',
    'part1.png',
    'part2.png',
    'part3.png',
    'part4.png',
    'part5.png',
    'p3-part1-hover.png',
    'p3-part2-hover.png',
    'p3-part3-hover.png',
    'p3-part4-hover.png',
    'p3-part5-hover.png',
  ],
  p4: [
    'stage-p4.png',
    'p4_1.png',
    'p4_2.png',
    'p4_3.png',
    'p4_4.png',
    'p4_4_5.png',
    'p4_5.png',
    'p4_6.png',
    'p4_7.png',
    'p4_8.png',
  ],
  p5: ['stage-p5.png', 'p5_1.png', 'p5_2.png', 'p5_3.png', 'p5_4.png'],
};

export { stageList, imgPre, imgSources };
