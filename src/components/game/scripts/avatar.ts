import * as PIXI from 'pixi.js';

// (obj: any)
export default class MyAvatar extends PIXI.Container {
  private speedAnimation: number;
  private k: number;
  private a: number;
  private i: number;
  private stages: number[][];
  private initPosition: number[];
  private initScale: number; // 缩放比例
  private initWidth: number;
  private initHeight: number;
  private scaledWidth: number;
  private scaledHeight: number;
  private avatarUrl: string;
  private avatarWrap: string;

  constructor({
    initPosition = [100, 100],
    initScale,
    avatarUrl,
  }: {
    initPosition?: number[];
    initScale: number;
    avatarUrl: string;
  }) {
    super();
    this.avatarUrl = avatarUrl;
    this.initScale = initScale;
    this.avatarWrap =
      'https://qgt-style.oss-cn-hangzhou.aliyuncs.com/img/ice/user-map/common/avatar.png';
    this.initWidth = 114; // 头像宽度
    this.initHeight = 123; // 头像高度
    this.scaledWidth = this.initWidth * this.initScale; // 头像宽度
    this.scaledHeight = this.initHeight * this.initScale; // 头像高度
    this.initPosition = initPosition; // 头像初始位置
    this.speedAnimation = 0.25; // 整体运动速度
    this.k = 0.3; // 加速度斜率
    this.a = 2; // 加速度基数
    this.i = 1; // 开始点的序号
    this.stages = [
      [1, 1],
      [1.2, 0.8],
      [0.85, 1.15],
      [1.1, 0.9],
      [0.9, 1.1],
      [1, 1],
      [0.95, 1.05],
      [1, 1],
    ]; // 运动点

    this.position.set(this.initPosition[0], this.initPosition[1]);
    this.scale.set(this.initScale);

    // wrap
    const avatarWrap = PIXI.Sprite.from(this.avatarWrap);
    avatarWrap.anchor.set(0.5);
    avatarWrap.width = this.initWidth;
    avatarWrap.height = this.initHeight;
    this.addChild(avatarWrap);

    const avatarImage = PIXI.Sprite.from(this.avatarUrl);
    avatarImage.anchor.set(0.5);
    avatarImage.x = 0;
    avatarImage.y = -18 / 4;
    avatarImage.width = this.initWidth - 18 * 2;
    avatarImage.height = this.initWidth - 18 * 2;
    this.addChild(avatarImage);

    const shape = new PIXI.Graphics();
    shape.beginFill(0xfff000);
    // shape.drawCircle(this.x, this.y - 18 / 2, 72);
    shape.drawCircle(0, -(18 / 4), (this.initWidth - 18 * 2) / 2);
    shape.closePath();
    shape.endFill();
    // shape.position.set(this.x, this.y)
    this.addChild(shape);
    avatarImage.mask = shape;
  }

  update(position: number[]) {
    this.position.set(position[0], position[1]);
    this.i = 1;
  }

  animate() {
    let diff = true;
    const speed = this.calcSpeed();

    const current = this.stages[this.i];
    const prev = this.stages[this.i - 1];

    if (current[0] > prev[0]) {
      diff = false;
    }
    if (
      diff
        ? this.width > current[0] * this.scaledWidth
        : this.width < current[0] * this.scaledWidth
    ) {
      this.width += (current[0] - prev[0]) * this.scaledWidth * speed;
      this.height += (current[1] - prev[1]) * this.scaledHeight * speed;

      this.y -=
        (((current[1] - prev[1]) * this.initPosition[1] * this.initScale) / 4) *
        speed;
    } else if (this.i < this.stages.length - 1) {
      this.i++;
      this.a += this.i * this.k;
    } else {
      this.width = this.scaledWidth;
      this.height = this.scaledHeight;
    }
  }

  calcSpeed() {
    return this.a / (60 * this.speedAnimation);
  }
}
