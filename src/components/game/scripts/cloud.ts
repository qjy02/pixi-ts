import { stageList, imgPre } from "./config";
import * as PIXI from 'pixi.js';
var clouds = [
    {
        cloudSrc: "/common/cloud4.png",
        positionX: 300,
        positionY: 100,
        cloudScale: 0.3,
        cloudDirection: 1,
        cloudSpeed: 0.1,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud2.png",
        positionX: 150,
        positionY: 200,
        cloudScale: 0.26,
        cloudDirection: 1,
        cloudSpeed: 0.1,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud3.png",
        positionX: 300,
        positionY: 300,
        cloudScale: 0.3,
        cloudDirection: 1,
        cloudSpeed: 0.3,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud1.png",
        positionX: 100,
        positionY: 400,
        cloudScale: 0.3,
        cloudDirection: 1,
        cloudSpeed: 0.3,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud5.png",
        positionX: 500,
        positionY: 500,
        cloudScale: 0.16,
        cloudDirection: 0.26,
        cloudSpeed: 0.5,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud1.png",
        positionX: 800,
        positionY: 100,
        cloudScale: 0.2,
        cloudDirection: 1,
        cloudSpeed: 0.2,
        moveLength:200
    },
    {
        cloudSrc: "/common/cloud1.png",
        positionX: 800,
        positionY: 350,
        cloudScale: 0.3,
        cloudDirection: 1,
        cloudSpeed: 0.2,
        moveLength:200
    },

];

export default function cloudMove(app:any){
    for (let cloud of clouds) {
        const cloudMoveArea = new PIXI.Rectangle(
            cloud.positionX,cloud.positionY,cloud.moveLength,100
        );

        const cloudImg = PIXI.Sprite.from(imgPre+cloud.cloudSrc);
        cloudImg.x = cloud.positionX;
        cloudImg.y = cloud.positionY;
        
        cloudImg.scale.set(cloud.cloudScale);
        app.stage.addChild(cloudImg); 
        app.ticker.add(() => {
            cloudImg.x +=cloud.cloudDirection*cloud.cloudSpeed;
            if(cloudImg.x+cloudImg.width>cloudMoveArea.x+cloudMoveArea.width || cloudImg.x<cloudMoveArea.x){
                cloud.cloudDirection = -cloud.cloudDirection;
            }
            
        });
    }
    for (let cloud of clouds) {
        const cloudMoveArea = new PIXI.Rectangle(
            cloud.positionX+300,cloud.positionY,cloud.moveLength,100
        );

        const cloudImg = PIXI.Sprite.from(imgPre+cloud.cloudSrc);
        cloudImg.x = cloud.positionX+300;
        cloudImg.y = cloud.positionY+150;
        
        cloudImg.scale.set(cloud.cloudScale);
        app.stage.addChild(cloudImg); 
        app.ticker.add(() => {
            cloudImg.x +=cloud.cloudDirection*cloud.cloudSpeed;
            if(cloudImg.x+cloudImg.width>cloudMoveArea.x+cloudMoveArea.width || cloudImg.x<cloudMoveArea.x){
                cloud.cloudDirection = -cloud.cloudDirection;
            }
            
        });
    }

}

