export interface StageList {
  stages: Array<MapStage>;
}

export interface MapStage {
  id: string; //pro_p2
  stageType: string; //阶段/P2
  stageName: string; //阶段名称/爬虫工程师
  stageCourses: Array<MapCourse>; // 阶段课程
  isdeveloping: boolean; //课程是否在开发中
  bought: boolean; //是否购买
  source: any;
  expiry?: boolean;
}

export interface MapCourse {
  id: string; //课程id/pro_j1
  courseName: string; //课程名称
  // courseChapters: Array<number>; //课程的章[1,2,3,4]
  // currentChapter: number; //当前所在章
  isLocked: boolean; //是否锁定（锁定的有阴影）
  isPreSale: boolean; //是否为预售课程
  courseId: string;
  myCourse?: MapMyCourse;
}

interface MapMyCourse {
  learnUrl?: string;
  process?: number; // 学习进度
  expiry?: boolean; //是否在有效期
}
