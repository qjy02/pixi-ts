function setCookie(cname: string, cvalue: string, exdays: number) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = 'expires=' + d.toLocaleString();
  document.cookie = cname + '=' + cvalue + '; ' + expires;
}
function getCookie(cname: string): string {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i].trim();
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}
export default function checkCookie() {
  var firstView = getCookie('isFirst');
  if (firstView !== '') {
    return false;
  } else {
    firstView = 'firstView';
    setCookie('isFirst', firstView, 30);
    return true;
  }
}
