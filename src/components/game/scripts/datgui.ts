import * as dat from 'dat.gui';

const guiText = {
  message: 'dat.gui',
  speed: 0.8,
  displayOutline: false,
  scale: 1,
  x: 0,
  y: 0,
};

export default function initGUI(container: HTMLElement, obj: any) {
  const gui = new dat.GUI();
  // console.log(container, obj.sprites)
  // if (container) {
  //   container.appendChild(gui.domElement);
  // }
  // gui.domElement.style.position = 'fixed'
  // gui.domElement.style.top = '50px'
  // gui.domElement.style.right = '50px'

  // p2_1
  const p2 = gui.addFolder('stage p2');

  // const p2_1 = gui.addFolder('stage p2_1')

  // p2
  // hover position
  const p2_x_controller = p2
    .add(guiText, 'x')
    .step(1)
    .listen();
  p2_x_controller.onChange((data: any) => {
    // obj.sprites.stages['stage-p3'].background.x = data;
    // console.log('0------x', obj.sprites.stages['stage-p2'].textContainer);
    // obj.sprites.stages['stage-p3'].textContainer.x = data;
    // obj.sprites.stages['stage-p2'].lockStage.x = data;
    // obj.sprites.stages['stage-p2'].background.x = data;
    obj.sprites.stages['stage-p2'].part1.lockMask.x = data;
  });
  const p2_y_controller = p2
    .add(guiText, 'y')
    .step(1)
    .listen();
  p2_y_controller.onChange((data: any) => {
    // console.log('0------y', obj.sprites.stages['stage-p2'].textContainer);
    // obj.sprites.stages['stage-p3'].background.y = data;
    // obj.sprites.stages['stage-p3'].textContainer.y = data;
    // obj.sprites.stages['stage-p2'].lockStage.y = data;
    // obj.sprites.stages['stage-p2'].background.y = data;
    obj.sprites.stages['stage-p2'].part1.lockMask.y = data;
  });

  // scale
  const scale_controller = p2
    .add(guiText, 'scale', 0, 1)
    .step(0.01)
    .listen();
  scale_controller.onChange((data: any) => {
    // obj.sprites.stages['stage-p3'].background.scale.set(data);
    // obj.sprites.stages['stage-p3'].textContainer.scale.set(data);
    // obj.sprites.stages['stage-p2'].wave3.scale.set(data);
    // obj.sprites.stages['stage-p2'].background.scale.set(data);
  });
}

export { guiText };
