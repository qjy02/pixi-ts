import { guiText } from '../scripts/datgui';

export default function dragSprite(sprite: any, setAnchor = true) {
  // console.log(sprite)
  setAnchor && sprite.anchor.set(0.5);
  sprite.interactive = true;
  sprite.buttonMode = true;
  sprite
    .on('pointerdown', onDragStart)
    .on('pointerup', onDragEnd)
    .on('pointerupoutside', onDragEnd)
    .on('pointermove', onDragMove);
}

function onDragStart(event: any) {
  console.log(event.currentTarget, 'event------');
  if (event.target) {
    event.target.data = event.data;
    event.target.alpha = 0.5;
    event.target.dragging = true;
    // event.target.anchor.set(0.5)
  }
}
function onDragEnd(event: any) {
  if (event.target) {
    event.target.alpha = 1;
    event.target.dragging = false;
    // set the interaction data to null
    event.target.data = null;
    // event.target.anchor.set(0)
  }
}

function onDragMove(event: any) {
  if (event.target && event.target.dragging) {
    // console.log(event, 'event Move------')
    const newPosition = event.target.data.getLocalPosition(event.target.parent);
    event.target.x = newPosition.x;
    event.target.y = newPosition.y;

    guiText.x = newPosition.x;
    guiText.y = newPosition.y;
  }
}
