import { imgPre } from './config';
import * as PIXI from 'pixi.js';
export default function buyCourse(stageType: string, lockPosition: any) {
  const container = new PIXI.Container();

  container.position.set(lockPosition[0], lockPosition[1]);

  const message1 = new PIXI.Text('你还未能解锁课程', {
    fontSize: 18,
    fill: 0x333333,
    align: 'center',
    fontWeight: 'bold',
  });
  const message2 = new PIXI.Text('快去购买课程开始学习吧', {
    fontSize: 16,
    fill: 0x818181,
    align: 'center',
    fontWeight: 'normal',
  });
  const spriteBg = PIXI.Sprite.from(imgPre + '/buy/buy_bg.png');
  const spriteBtn = PIXI.Sprite.from(imgPre + '/buy/buy_btn.png');

  message1.position.set(spriteBg.width / 2 - message1.width / 2, 67);
  message2.position.set(spriteBg.width / 2 - message2.width / 2, 125);
  spriteBtn.position.set(20, 20);
  spriteBtn.position.set(spriteBg.width / 2 - spriteBtn.width / 2, 170);
  container.addChild(spriteBg, spriteBtn, message1, message2);
  container.zIndex = 1;
  spriteBtn.interactive = true;
  spriteBtn.buttonMode = true;
  spriteBtn.on('pointerdown', () => {
    window.open('https://www.youkeda.com/home_m/college');
    container.visible = false;
  });
  return container;
}
