import { stageList } from './config';
import * as PIXI from 'pixi.js';
const imgPre =
  'https://qgt-style.oss-cn-hangzhou.aliyuncs.com/img/ice/user-map';
const userGuide1 = PIXI.Sprite.from(imgPre + '/guide/1-2x.png');
const userGuide2 = PIXI.Sprite.from(imgPre + '/guide/2-2x.png');
const userGuide3 = PIXI.Sprite.from(imgPre + '/guide/3-2x.png');
const userGuide4 = PIXI.Sprite.from(imgPre + '/guide/4-2x.png');

const userGuide1Button = new PIXI.Graphics();
const userGuide2Button = new PIXI.Graphics();
const userGuide3Button = new PIXI.Graphics();
const userGuide4Button = new PIXI.Graphics();

export default function userGuide(app: any) {
  /**************** 新手引导蒙层*********************/
  const userGuideMask = new PIXI.Graphics();
  userGuideMask.beginFill(0x000000, 0.6);
  userGuideMask.drawRect(0, 0, app.view.width, app.view.height);
  userGuideMask.endFill();
  app.stage.addChild(userGuideMask);
  /************************ 新手引导一*****************/
  userGuide1.x = app.stage.width / 2;
  userGuide1.y = app.stage.height / 2 + 40;
  userGuide1.scale.set(0.5);
  userGuide1.anchor.set(0.5);
  userGuide1.interactive = false;
  app.stage.addChild(userGuide1);

  const userGuide1Button = new PIXI.Graphics();
  userGuide1Button.beginFill(0xffffff, 0.01);
  userGuide1Button.drawRect(userGuide1.x - 40, userGuide1.y + 125, 90, 30);
  userGuide1Button.endFill();
  userGuide1Button.interactive = true;
  userGuide1Button.buttonMode = true;
  userGuide1Button.on('pointerdown', function() {
    userGuide1.visible = false;
    userGuide1Button.visible = false;
    userGuide2.visible = true;
    userGuide2Button.visible = true;
  });
  app.stage.addChild(userGuide1Button);
  /***********************新手引导二 ***************/
  userGuide2.x = app.stage.width / 2;
  userGuide2.y = app.stage.height / 2 + 40;
  userGuide2.anchor.set(0.5);
  userGuide2.scale.set(0.5);
  // userGuide2.interactive = false;
  app.stage.addChild(userGuide2);
  userGuide2.visible = false;
  userGuide2Button.visible = false;
  userGuide2Button.on('pointerdown', function() {
    userGuide2.visible = false;
    userGuide2Button.visible = false;
    userGuide3.visible = true;
    userGuide3Button.visible = true;
  });
  app.stage.addChild(userGuide2Button);
  /*************************新手引导三************* */
  userGuide3.x = app.stage.width / 2 - 205;
  userGuide3.y = app.stage.height / 2 - 255;
  userGuide3.scale.set(0.5);
  userGuide3.interactive = false;
  userGuide3.visible = false;
  app.stage.addChild(userGuide3);

  userGuide3Button.visible = false;
  userGuide3Button.on('pointerdown', function() {
    userGuide3.visible = false;
    userGuide3Button.visible = false;
    userGuide3Button.on('pointerdown', function() {
      userGuide3.visible = false;
      userGuide3Button.visible = false;
      userGuide4.visible = true;
      userGuide4Button.visible = true;
    });
    app.stage.addChild(userGuide3Button);
    /*************************新手引导四****************/
    userGuide4.x = app.stage.width / 2 - 229;
    userGuide4.y = app.stage.height / 2 - 260;
    userGuide4.scale.set(0.5);
    userGuide4.interactive = false;
    userGuide4.visible = false;
    userGuide4Button.visible = false;
    userGuideMask.visible = false;
  });
  app.stage.addChild(userGuide4Button);
}
