import * as PIXI from 'pixi.js';
import { imgPre } from './config';

export function createOverlay(obj: any) {
  const texture = obj.resource[`${imgPre}/common/overlay.png`].texture;
  const displacementTexture =
    obj.resource[`${imgPre}/common/replacement.png`].texture;
  displacementTexture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
  const displacementSprite = new PIXI.Sprite(displacementTexture);

  const width = obj.app.view.width;
  const height = obj.app.view.height;

  const overlay = new PIXI.TilingSprite(texture, width * 1.5, height * 1.5);
  overlay.scale.set(2 / 3);
  overlay.alpha = 0.5;

  const filter = new PIXI.filters.DisplacementFilter(displacementSprite, 200);
  overlay.filters = [filter];
  return overlay;
}
